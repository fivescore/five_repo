import React, { Component } from 'react';
import * as ReactBootstrap from 'react-bootstrap';
require("react-bootstrap/lib/NavbarHeader")
var homeTeam;
var awayTeam;
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { match: [homeTeam,awayTeam], 
                  user: '',
                  auth: false }; // <- set up react state
  }
onClickHandler(clicked){
  this.props.viewChange(clicked);
}

  componentWillReceiveProps(nextProps) {
    this.setState({
        user: nextProps.user,
        auth: nextProps.auth
    });
  }

render(){
  return (
    <ReactBootstrap.Navbar inverse collapseOnSelect>
      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css"/>
    <ReactBootstrap.Navbar.Header>
      <ReactBootstrap.Navbar.Brand>
        Five Score
      </ReactBootstrap.Navbar.Brand>
      <ReactBootstrap.Navbar.Toggle />
    </ReactBootstrap.Navbar.Header>
    <ReactBootstrap.Navbar.Collapse>
      <ReactBootstrap.Nav>
          <ReactBootstrap.NavItem eventkey={3.1}  onClick={() => this.onClickHandler('match')}>Match List</ReactBootstrap.NavItem>
          {this.props.auth ?<ReactBootstrap.NavItem eventKey={3.2}  onClick={() => this.onClickHandler('user')}>User List</ReactBootstrap.NavItem>:null}
          <ReactBootstrap.NavItem eventkey={3.3} onClick={() => this.onClickHandler('table')}>Table</ReactBootstrap.NavItem>
          <ReactBootstrap.MenuItem divider />
      </ReactBootstrap.Nav>
      <ReactBootstrap.Nav pullRight>
      {this.props.auth ? <ReactBootstrap.NavItem id='name'>{this.state.user}</ReactBootstrap.NavItem> : null}
      {!this.props.auth ? 
      <ReactBootstrap.NavItem eventkey={2} onClick={this.props.onClickLogin.bind(this)}>Login</ReactBootstrap.NavItem>:
      <ReactBootstrap.NavItem eventkey={1} onClick={this.props.onClickLogout.bind(this)}>Logout</ReactBootstrap.NavItem>}  
       
        
      </ReactBootstrap.Nav>
    </ReactBootstrap.Navbar.Collapse>
  </ReactBootstrap.Navbar>
  );
}
}

export default Header;