import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';

// CSS Modules, react-datepicker-cssmodules.css
// import 'react-datepicker/dist/react-datepicker-cssmodules.css';

class Date extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      startDate: moment()
    };
    this.handleChange = this.handleChange.bind(this);
    this.handlePick = this.handlePick.bind(this);
  }

  handleChange(date) {
    this.setState({
      startDate: date
    });
  }

  handlePick(){
    this.props.selectDate(this.state.startDate)
  }
  

  
  render() {
    return (
    <div>
    <DatePicker
        selected={this.state.startDate}
        onChange={this.handleChange}
    />
    {this.handlePick()}</div>
    )
  }
}

export default Date;