import React, { Component } from 'react';
import fire from '.././fire';
import UserListTable from './UserListTable.js'
import '../App.css';
require("react-bootstrap/lib/NavbarHeader")

class Table extends Component {

    constructor(props) {
        super(props);

        this.state = {
            matches: {
                team1: "",
                team2: "",
                team1goal: 0,
                team2goal: 0,
                matchId: 0
            },
            loading: true,
            users: {
                name: "",
                bettings: {},
                points: 0
            }
        }
    }
    componentDidMount() {
        this.database1 = fire.database().ref().child('matches');

        // <- set up react state
        this.setState({ loading: true })

        this.database1.on("value", snap => {
            let that = this;
            let matches = [];
            snap.forEach(function (data) {
                let match = {
                    team1: data.val().team1,
                    team2: data.val().team2,
                    team1goal: data.val().team1goal,
                    team2goal: data.val().team2goal,
                    matchId: data.key
                }
                //console.log(match.matchId);
                matches.push(match);
                that.setState({
                    matches: matches,
                });
            });
        });
        this.database2 = fire.database().ref().child('user');
        this.setState({ loading: true })
        this.database2.on("value", snap => {
            let users = [];
            let that = this;
            snap.forEach(function (data) {
                let user = {
                    name: data.val().name,
                    bettings: data.val().betting,
                    points: 0
                }
                users.push(user)
                that.setState({
                    users: users,
                });

            });
            this.setState({
                loading: false
            })
        })
    }

    render() {
        if (!this.state.loading) {
            //console.log(this.state.users[0].bettings);
            return (
                <UserListTable
                    matches={this.state.matches}
                    users={this.state.users}
                    loading={this.state.loading}
                    view={this.props.view}
                />
            )
        }
        return (null)
    }
}

export default Table;