import React, { Component } from 'react';
import fire from '.././fire';
import Date from './DatePicker';
import moment from 'moment';
//import loadFromServer from '../service/webservice';
import '../App.css';
require("react-bootstrap/lib/NavbarHeader")

const CustomRow = (Time, Teams, Score, i, viewBetting, auth) => {
    return (
        <tr key={i++}>
            <td id='time'>{Time}</td>
            <td id='team'>{Teams[0]}</td>
            <td id='score'> {Score[0]} - {Score[1]}</td>
            <td id='team'>{Teams[1]}</td>
            {auth ? <td id='bet'><button onClick={() => viewBetting(i - 1)}>Betting</button></td> : null}
        </tr>
    );
}

const CustomRow2 = (i, shows, saveBetting, matchId,auth) => {
    if (shows && auth) {
        return (
            <tr>
                <td id='time'></td>
                <td id='team'><button onClick={() => saveBetting(1, matchId)}>1</button></td>
                <td id='score'><button onClick={() => saveBetting('X', matchId)}>X</button></td>
                <td id='team'><button onClick={() => saveBetting(2, matchId)}>2</button></td>
            </tr>
        );
    }
    //return null;
}


class Matches extends Component {

    constructor(props) {
        super(props);
        this.viewBetting = this.viewBetting.bind(this)
        this.saveBetting = this.saveBetting.bind(this)
        this.database = fire.database().ref().child('matches');

        this.state = {
            matches: {
                matchId: 0,
                teams: [],
                score: [],
                shows: false,
                auth: this.props.auth,
                date: ''
            },
            loading: true,
            selectedDate: moment(),

        };
    }

    viewBetting(i) {
        //console.log(auth)
    
            let match = this.state.matches;
            match[i].shows = !match[i].shows;
            this.setState({
                matches: match
            });
        }


    componentWillReceiveProps(nextProps){
        this.setState({
            auth: nextProps.auth
        })
    }

    saveBetting(bet, matchId) {
        let time = 0;
        this.database0 = fire.database().ref().child('matches').child(matchId);
        this.database0.on("value", snap => {
            time = snap.val().time;
        });
        let currentDate = moment();
        let date = time.split('.');
        let day = date[2];
        let month = date[1];
        let year = date[0];
        let hour = date[3];
        let hours = hour.split(':');
        if (currentDate._d.getFullYear().toString() < year) {
            this.database1 = fire.database().ref().child('user').child(this.props.user).child('betting').child(matchId).set(bet);
        }
        if (currentDate._d.getFullYear().toString() === year) {
            if ((currentDate._d.getMonth() + 1).toString() < month) {
                this.database1 = fire.database().ref().child('user').child(this.props.user).child('betting').child(matchId).set(bet);
            }
            if ((currentDate._d.getMonth() + 1).toString() === month) {
                if (currentDate._d.getDate().toString() < day) {
                    this.database1 = fire.database().ref().child('user').child(this.props.user).child('betting').child(matchId).set(bet);
                }
                if (currentDate._d.getDate().toString() === day) {
                    if (currentDate._d.getHours() < hours[0].substring(1, 3)) {
                        this.database1 = fire.database().ref().child('user').child(this.props.user).child('betting').child(matchId).set(bet);
                    }
                    if (currentDate._d.getHours() === hours[0].substring(1, 3) && currentDate._d.getMinutes() <= hours[1]) {
                        this.database1 = fire.database().ref().child('user').child(this.props.user).child('betting').child(matchId).set(bet);
                    }
                }
            }
        }
    }

    componentDidMount() {

        this.setState({ loading: true })
        this.database.on("value", snap => {
            let that = this;
            let matches = []
            snap.forEach(function (data) {
                let match = {
                    matchId: data.key,
                    teams: [data.val().team1, data.val().team2],
                    score: [data.val().team1goal, data.val().team2goal],
                    date: data.val().time,
                    shows: false
                }
                matches.push(match);
                that.setState({
                    matches: matches,
                });
            });
            this.setState({ loading: false })
        });
    }

    selectDate(newDate) {
        if (this.state.selectedDate !== newDate) {
            this.setState({
                selectedDate: newDate
            })
        }
    }

    render() {
        if (this.props.view) {
            if (this.state.loading) {
                return <h2>Loading...</h2>;
            }
            let rows = [];
            let i = 0;
            for (i = 0; i < Object.keys(this.state.matches).length; i++) {
                let date = this.state.matches[i].date.split('.');
                let day = date[2];
                let month = date[1];
                let year = date[0];
                if (this.state.selectedDate._d.getFullYear().toString() === year &&
                    (this.state.selectedDate._d.getMonth() + 1).toString() === month &&
                    this.state.selectedDate._d.getDate().toString() === day) {
                     rows.push(CustomRow(this.state.matches[i].date.substring(12,17), this.state.matches[i].teams, this.state.matches[i].score, i, this.viewBetting, this.props.auth));
                    rows.push(CustomRow2(i, this.state.matches[i].shows, this.saveBetting, this.state.matches[i].matchId,this.state.auth));
                   
    
                }
            };
            if (Object.keys(rows).length === 0) {
                return (
                    <div>
                        <div id='date-picker'> <Date selectDate={this.selectDate.bind(this)} /> 
                        </div>
                        <h1 id='matchless'>In the selected date we don't have match in the database!</h1>
                        <h1 id='matchless'>Try another date!</h1>
                    </div>
                );
            }
            return (
                <div>
                    <div id='date-picker'> <Date selectDate={this.selectDate.bind(this)} /> </div>
                    <h3 id='matches-head'>Fixtures</h3>
                    <table className='table table-stripped' id='fixTable'>
                        <tbody>{rows}</tbody>
                    </table>
                </div>
            );
        }
        return (null);
    }
};

export default Matches;