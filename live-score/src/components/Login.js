import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import "../App.css";
import fire from '.././fire';
import { Intent } from '@blueprintjs/core'
import Register from './Register'

const loginStyles = {
  width: "90%",
  maxWidth: "315px",
  margin: "20px auto",
  borderRadius: "5px",
  padding: "10px"
}

class Login extends Component {
  constructor(props) {
    super(props);
    this.authWithEmailPassword = this.authWithEmailPassword.bind(this)
    this.state = {
      show: false,
      controller: false,
      register: false,
      auth: false
    }; 
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      auth: nextProps.auth,
      show: nextProps.show
    })
  }

  authWithEmailPassword(event) {
    event.preventDefault()

    const email = this.emailInput.value
    const password = this.passwordInput.value

    fire.auth().fetchProvidersForEmail(email)
      .then((providers) => {
        if (providers.length === 0) {
          // create user
          //return fire.auth().createUserWithEmailAndPassword(email, password)
          alert('no such user');
        } else if (providers.indexOf("password") === -1) {
          // they used facebook
          this.loginForm.reset()
          this.toaster.show({ intent: Intent.WARNING, message: "Try alternative login." })
        } else {
          // sign user in
          return fire.auth().signInWithEmailAndPassword(email, password)
        }
      })
      .then((user) => {
        if (user && user.email) {
          this.loginForm.reset()
          this.props.setCurrentUser(user)
          this.setState({ redirect: !this.state.redirect })
        }
      })
      .catch((error) => {
        alert(error.message)
      })
  }

  render() {
    if (this.props.show !== this.state.show) {
      this.setState({
        show: this.props.show,
        controller: false,
      })
    }
    if (!this.state.auth) {
      if (!this.state.register) {
        return (
          <Modal
            show={this.state.show}
            onHide={this.props.close}
            container={this}
            aria-labelledby="contained-modal-title"
            style={loginStyles}
          >
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body>
              <h2>Login</h2>
              <form onSubmit={(event) => { this.authWithEmailPassword(event) }} ref={(form) => { this.loginForm = form }}>
                <div>
                  <label className="pt-label">
                    Email

            <input style={{ width: "100%" }} className="pt-input" name="email" type="email" ref={(input) => { this.emailInput = input }} placeholder="Email"></input>
                  </label>
                </div>
                <div>
                  <label className="pt-label">
                    Password

            <input style={{ width: "100%" }} className="pt-input" name="password" type="password" ref={(input) => { this.passwordInput = input }} placeholder="Password"></input>

                  </label>
                </div>
                <input style={{ width: "50%" }} type="submit" className="pt-button pt-intent-primary" value="Log In"></input>

              </form>
              <hr />
              <button onClick={() => this.setState({ register: true })}>Register</button>
            </Modal.Body>
          </Modal>
        );
      }
      else if (this.state.register) {
        this.state.register = false;
        return (
          <Modal
            show={this.state.show}
            onHide={this.props.close}
            container={this}
            aria-labelledby="contained-modal-title"
            style={loginStyles}
          >
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body>
              <Register setCurrentUser={this.props.setCurrentUser} />
            </Modal.Body>
          </Modal>
        );
      }
    }
    return (null);
  }
}

export default Login;