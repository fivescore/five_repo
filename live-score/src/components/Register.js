import React, { Component } from "react";
import "../App.css";
import fire from '.././fire';
/*
const loginStyles = {
  width: "90%",
  maxWidth: "315px",
  margin: "20px auto",
  borderRadius: "5px",
  padding: "10px"
}*/


class Register extends Component {
  constructor(props) {
    super(props);
    this.authWithEmailPassword = this.authWithEmailPassword.bind(this)
    this.state = {
      show: false,
      controller: false,
      register: false
    };
  }


  authWithEmailPassword(event) {
    event.preventDefault()
    if (this.passwordInput.value === this.passwordInput2.value) {
      const password = this.passwordInput.value

      let use = {
        email: this.emailInput.value,
        userName: this.nameInput.value,
        betting: []
      }

      fire.auth().fetchProvidersForEmail(use.email)
        .then((providers) => {
          if (providers.length === 0) {
            //create user
            return fire.auth().createUserWithEmailAndPassword(use.email, password)
          }
        })
        .then((user) => {
          if (user && user.email) {
            this.database = fire.database().ref().child('user').child(user.uid);
            this.database.child('name').set(use.userName);
            this.database.child('email').set(use.email);
            this.loginForm.reset()
            this.props.setCurrentUser(user)
            this.setState({ redirect: true })
          }
        })
        .catch((error) => {
          alert(error.message)
        })
    }
    else {
      alert("Confirm Password failed!")
    }
  }

  render() {
    if (this.props.show !== this.state.show) {
      this.setState({
        show: this.props.show,
        controller: false,
      }
      )
    }
    return (
      <div>
        <h2>Register</h2>
        <form onSubmit={(event) => { this.authWithEmailPassword(event) }} ref={(form) => { this.loginForm = form }}>
          <div>
            <label className="pt-label">
              User Name
            <input style={{ width: "100%" }} className="pt-input" ref={(input) => { this.nameInput = input }} placeholder="UserName"></input>
            </label>
          </div>
          <div>
            <label className="pt-label">
              Email
            <input style={{ width: "100%" }} className="pt-input" name="email" type="email" ref={(input) => { this.emailInput = input }} placeholder="Email"></input>
            </label>
          </div>
          <div>
            <label className="pt-label">
              Password
            <input style={{ width: "100%" }} className="pt-input" name="password" type="password" ref={(input) => { this.passwordInput = input }} placeholder="Password"></input>
            </label>
            <label className="pt-label">
              Confirm Password
            <input style={{ width: "100%" }} className="pt-input" name="password" type="password" ref={(input) => { this.passwordInput2 = input }} placeholder="Confirm Password"></input>
            </label>
          </div>
          <input style={{ width: "50%" }} type="submit" className="pt-button pt-intent-primary" value="Register"></input>
        </form>
      </div>
    );
  }
}

export default Register;