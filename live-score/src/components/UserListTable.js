import React, { Component } from 'react';
import '../App.css';
require("react-bootstrap/lib/NavbarHeader")

const CustomRow = (user, i) => {
    //this.tableResolve()
    return (<tr key={i++}>
        <td id='position'>{i}</td>
        <td id='team1'>{user.name}</td>
        <td id='points'>{user.points}</td>
    </tr>
    );
}

class UserListTable extends Component {

    constructor(props) {
        super(props);

        this.state = {
            users: this.props.users,
            matches: this.props.matches
        }

    }


    componentWillReceiveProps(nextProps) {
        this.setState({
            matches: nextProps.matches,
            users: nextProps.users
        });
        this.tableResolve(nextProps.matches, nextProps.users);
    }

    componentWillMount() {
        this.tableResolve(this.state.matches, this.state.users);
    }


    makeitnull() {
        let temp = this.state.users
        for (var i = 0; i < Object.keys(temp).length; i++) {
            temp[i] = {
                name: temp[i].name,
                bettings: temp[i].bettings,
                points: 0
            }
        }

        this.state.users = temp
    }


    tableResolve(matches, users) {
        this.makeitnull()
        for (var k = 0; k < Object.keys(users).length; k++) {
            for (var j = 0; j < Object.keys(matches).length; j++) {
                let matchIdToBettings = matches[j].matchId.toString();
                let theBet;
                if (users[k].bettings) {
                    if (users[k].bettings.hasOwnProperty(matchIdToBettings)) {
                        theBet = users[k].bettings[matchIdToBettings].toString();
                    }
                    else {
                        continue;
                    }
                    if (!isNaN(matches[j].team1goal) && !isNaN(matches[j].team2goal)) {
                        switch (theBet) {
                            case "1":
                                if (matches[j].team1goal > matches[j].team2goal) {
                                    users[k].points += 2;
                                } else {
                                    users[k].points -= 1;
                                }
                                break;
                            case "2":
                                if (matches[j].team1goal < matches[j].team2goal) {
                                    users[k].points += 2;
                                } else {
                                    users[k].points -= 1;
                                }
                                break;
                            case "X":
                                if (matches[j].team1goal === matches[j].team2goal) {
                                    users[k].points += 2;
                                } else {
                                    users[k].points -= 1;
                                }
                                break;
                            default: break;
                        }
                    }
                }
            }
            function compare(a, b) {
                if (a.points < b.points)
                    return 1;
                if (a.points > b.points)
                    return -1;
                if (a.bettings && b.bettings) {
                    if (a.bettings.length < b.bettings.length)
                        return 1;
                    if (a.bettings.length > b.bettings.length)
                        return -1;
                }
                if (a.teamName < b.teamName)
                    return -1;
                if (a.teamName > b.teamName)
                    return 1;
                return 0;
            }

            users.sort(compare);
            this.setState({
                users: users
            });
        }
    }

    render() {
        if (this.props.view) {
            if (this.props.loading) {
                return <h2>Loading...</h2>;
            }
            else if (!this.props.loading) {
                let rows = [];
                let i = 0;
                for (i = 0; i < Object.keys(this.state.users).length; i++) {
                    rows.push(CustomRow(this.state.users[i], i));
                }


                if (this.props.view) {
                    if (!this.state.loading) {
                        return (
                            <div>
                                <h3 id='table-head'>User Table</h3>
                                <table className='table table-stripped' id='fixTable'>
                                    <thead><tr>
                                        <td id='position'></td>
                                        <td id='team1'>Name</td>
                                        <td id='points'>Points</td>
                                    </tr></thead>
                                    <tbody>{rows}</tbody>
                                </table>
                            </div>
                        )
                    }
                    return (null)
                }
                return (null)
            };

        } return (null);
    }
}

export default UserListTable;