import React, { Component } from 'react';
import fire from '.././fire';
import TableDataBase from './TableDataBase';
import '../App.css';
require("react-bootstrap/lib/NavbarHeader")

class Table extends Component {

    constructor(props) {
        super(props);

        this.state = {
            matches: {
                team1: "",
                team2: "",
                team1goal: 0,
                team2goal: 0,
            },
            loading: true,
            teamList: {
                teamName: "",
                matchPlayed: 0,
                goals: 0,
                goalsAgainst: 0,
                points: 0,
            }
        }
    }
    componentDidMount() {
        this.database1 = fire.database().ref().child('matches');

        // <- set up react state
        this.setState({ loading: true })

        this.database1.on("value", snap => {
            let that = this;
            let matches = [];
            snap.forEach(function (data) {
                let match = {
                    team1: data.val().team1,
                    team2: data.val().team2,
                    team1goal: data.val().team1goal,
                    team2goal: data.val().team2goal,
                    //date: data.val().time
                }
                matches.push(match);
                that.setState({
                    matches: matches,
                });
            });
        });
        this.database2 = fire.database().ref().child('teams');
        this.setState({ loading: true })
        this.database2.on("value", snap => {
            let teamList = [];
            let that = this;
            snap.forEach(function (data) {
                let team = {
                    teamName: data.val(),
                    matchPlayed: 0,
                    goals: 0,
                    goalsAgainst: 0,
                    points: 0,
                }

                teamList.push(team)
                that.setState({
                    teamList: teamList,
                });

            });
            this.setState({
                loading: false
            })
        })
    }

    render() {
        if (!this.state.loading) {
            return (
                <TableDataBase
                    matches={this.state.matches}
                    teamList={this.state.teamList}
                    loading={this.state.loading}
                    view={this.props.view}
                />
            )
        }
        return (null)
    }
}

export default Table;