import React, { Component } from 'react';
import '../App.css';
require("react-bootstrap/lib/NavbarHeader")

const CustomRow = (team, i) => {
    //this.tableResolve()
    return (<tr key={i++}>
        <td id='position'>{i}</td>
        <td id='team1'>{team.teamName}</td>
        <td id='played'> {team.matchPlayed}</td>
        <td id='score1'> {team.goals} </td>
        <td id='score2'> {team.goalsAgainst}</td>
        <td id='points'>{team.points}</td>
    </tr>
    );
}

class TableDataBase extends Component {

    constructor(props) {
        super(props);

        this.state = {
            teamList: this.props.teamList,
            matches: this.props.matches
        }

    }


    componentWillReceiveProps(nextProps) {
        this.setState({
            matches: nextProps.matches,
            teamList: nextProps.team
        });
        this.tableResolve(this.state.matches, this.state.teamList)
    }


    makeitnull() {
        let temp = this.state.teamList
        for (var i = 0; i < Object.keys(temp).length; i++) {
            temp[i] = {
                teamName: temp[i].teamName,
                matchPlayed: 0,
                goals: 0,
                goalsAgainst: 0,
                points: 0,
            }
        }
        this.state.teamList = temp
    }

    tableResolve(matches, teamList) {
        this.makeitnull()
        for (var i = 0; i < Object.keys(teamList).length; i++) {
            for (var j = 0; j < Object.keys(matches).length; j++) {
                if (matches[j].team1 === teamList[i].teamName) {
                    if (!isNaN(matches[j].team1goal)) {
                        teamList[i].matchPlayed++;
                        teamList[i].goals += matches[j].team1goal
                    }
                    if (!isNaN(matches[j].team2goal)) {
                        teamList[i].goalsAgainst += matches[j].team2goal;
                    }
                    if (matches[j].team1goal > matches[j].team2goal) {
                        teamList[i].points += 3;
                    } else if (matches[j].team1goal === matches[j].team2goal) {
                        if (!isNaN(matches[j].team1goal) && !isNaN(matches[j].team2goal)) {
                            teamList[i].points++;
                        }
                    }
                } else if (matches[j].team2 === teamList[i].teamName) {
                    if (!isNaN(matches[j].team2goal)) {
                        teamList[i].matchPlayed++;
                        teamList[i].goals += matches[j].team2goal;
                    }
                    if (!isNaN(matches[j].team1goal)) {
                        teamList[i].goalsAgainst += matches[j].team1goal;
                    }
                    if (matches[j].team2goal > matches[j].team1goal) {
                        teamList[i].points += 3;
                    } else if (matches[j].team1goal === matches[j].team2goal) {
                        if (!isNaN(matches[j].team1goal) && !isNaN(matches[j].team2goal)) {
                            teamList[i].points++;
                        }
                    }
                }
            }
        }
        function compare(a, b) {
            if (a.points < b.points)
                return 1;
            if (a.points > b.points)
                return -1;
            if (a.goals - a.goalsAgainst > b.goals - b.goalsAgainst)
                return -1;
            if (a.goals - a.goalsAgainst < b.goals - b.goalsAgainst)
                return 1;
            if (a.goals < b.goals)
                return 1;
            if (a.goals > b.goals)
                return -1;
            if (a.teamName < b.teamName)
                return -1;
            if (a.teamName > b.teamName)
                return 1;
            return 0;
        }

        teamList.sort(compare);

        this.setState({
            teamList: teamList
        });
    }
    render() {

        if (this.props.view) {
            if (this.props.loading) {
                return <h2>Loading...</h2>;
            }
            else if (!this.props.loading) {
                let rows = [];
                let i = 0;
                //this.tableResolve;
                //()=>this.tableResolve();
                for (i = 0; i < Object.keys(this.state.teamList).length; i++) {
                    rows.push(CustomRow(this.state.teamList[i], i));
                }


                return (
                    <div>
                        <h3 id='table-head'>Table</h3>
                        <table className='table table-stripped' id='fixTable'>
                            <thead><tr>
                                <td id='position'></td>
                                <td id='team1'>Team</td>
                                <td id='played'> Match Played</td>
                                <td id='score1'> Goals scored </td>
                                <td id='score2'> Goals Against</td>
                                <td id='points'>Points</td>
                            </tr></thead>
                            <tbody>{rows}</tbody>
                        </table>
                    </div>
                )
            };

        } return (null);
    }
}

export default TableDataBase;