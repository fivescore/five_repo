import React, { Component } from 'react';
import fire from './fire'
import './App.css'
import Login from './components/Login.js'; 
import Header from './components/Header'
import Matches from './components/Matches'
import Table from './components/Table'
import UserList from './components/UserList'
require("react-bootstrap/lib/NavbarHeader")

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {login: false,
        view: {
          user: '',
          userId: '',
          match: true,
          table: false,
          authenticated: false,
        }
                }; // <- set up react state
  }


onClickLogout(){
  this.setState({authenticated: false, login: false})
  this.viewChange('match'); 
  return(
   alert("Logged out")
  )
}
onClickLogin () {
  this.setState({login : true})
}

onClose() {
  this.setState({login: false})
}

setCurrentUser(user) {
  if (user) {

    this.setState({
      user: user.email,
      userId: user.uid,
      authenticated: true
    })
  } else {
    this.setState({
      user: null,
      userId: null,
      authenticated: false
    })
  }
}

viewChange(clicked){
  let view=this.state.view;
  view.user=false;
  view.match=false;
  view.table=false;
  switch(clicked) {
    case 'table':
       view.table=true;
        break;
    case 'match':
        view.match= true;
        break;
    case 'user':
      view.user=true;
      break;
    default:
        view= false;
}
  this.setState({view: view})
  console.log(this.state.view)
}
render(){
  return (
    <div> 
  <Login 
    auth = {this.state.authenticated}
    show={this.state.login} 
    close={this.onClose.bind(this)}
    setCurrentUser={this.setCurrentUser.bind(this)}/>
  <Header 
    user = {this.state.user}
    auth = {this.state.authenticated}
    onClickLogin={this.onClickLogin.bind(this)}
    onClickLogout={this.onClickLogout.bind(this)}
    viewChange={this.viewChange.bind(this)}/>
  <Matches view={this.state.view.match} user={this.state.userId} auth={this.state.authenticated}/>
  <Table view={this.state.view.table}/>
  <UserList view={this.state.view.user}/>
  </div>
  )
}
  

  componentWillMount(){
    /* Create reference to messages in Firebase Database */
    let messagesRef = fire.database().ref('messages').orderByKey().limitToLast(100);
    messagesRef.on('child_added', snapshot => {
      /* Update React state when message is added at Firebase Database */
      let message = { text: snapshot.val(), id: snapshot.key };
      this.setState({ messages: [message].concat(this.state.messages) });
    })
  }
  addMessage(e){
    e.preventDefault(); // <- prevent form submit from reloading the page
    /* Send the message to Firebase */
    fire.database().ref('match').push( this.homeTeam.value, this.awayTeam.value  );
    //this.inputEl.value = ''; // <- clear the input
  }

}

export default App;