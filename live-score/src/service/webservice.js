import React from 'react';
import $ from 'jquery';
  
  const loadFromServer =  ()=> {
    $.ajax({
        headers: { 'x-crowdscores-api-key': 'd431ec0d23eb4ee597e2912dc278f2e9' },
        url: 'https://api.crowdscores.com/v1',
    dataType: 'json',
    cache: false,
    type: 'GET',
    success: function(data) {
      console.log(data)
    }.bind(this),
    error: function(xhr, status, err) {
      console.error(this.url, status, err.toString());
    }.bind(this)
  });
}

export default loadFromServer;